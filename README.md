# Projekto 2 dalis

Repozitorijoje pateikti programų tekstai, skirti sudaryti statistinius modelius
duomenims, pateikiamiems *data/data_munging/data_by_country_by_hour/* aplanke.
Pastarieji prieš tai sudaromi savarankiškai komandinės eilutės
ir Python funkcijų pagalba.

Statistiniai modeliai sudaromi automatiškai main.py programoje.
Joje, pagal pasirinktus duomenų pjūvius, atliekamas duomenų paruošimas
(skaidymas į apmokymo ir testavimo duomenų imtis, failų formatų konvertavimas),
statistinių modelių sudarymas ir jų metrikų įvertinimas.

Sudarant statistinius modelius ir siekiant išvengti modelių persimokymo,
naudojama Laso *L1* ir Tichonovo *L2* reguliarizacijos.
Reguliarizacijų parametrai *L1* ir *L2* nurodyti *L1s.json* ir *L2s.json*
failuose, tačiau gali būti koreguojami priklausomai nuo poreikio ir turimų
kompiuterinių resursų. Papildomai taip pat keičiamas ir modelių apmokymo
greitis (angl. learning rate), kurio kitimo diapazonas nurodytas
*learning_rates.json* faile.

Statistiniai modeliai sudaromi ir įvertinami iškvietus Linux komandas:
*python3 init.py data/model_parameters/slicing_params_710.json*
arba
*python3 init.py data/model_parameters/slicing_params_826.json*
arba
*python3 init.py data/model_parameters/slicing_params_840.json*
atitinkamai pagal tai, kurios valstybės (710, 826 ar 840)
ir kurio duomenų pjūvio duomenims sudaromas modelis.
*init.py* programa savo ruožtu iškviečia *main.py* programą,
kuri ir įvykdo aukščiau aprašytas operacijas.
