import itertools
import math
import subprocess

import click
from mpipe.bin import args_handlers


@click.group()
def mpipe_cli():
    pass


@mpipe_cli.command("rmse",
                   help="Compute Root Mean Squared Error (RMSE)")
@click.argument("file_y", type=click.File("r"), default="-")
@click.argument("file_y_hat", type=click.File("r"))
@click.option(
    "--output", "-o", type=click.File("w"), default="-",
    help="output file")
@click.option(
    "--delim-y", default=" ",
    help="FILE_Y column delimiter, default: ' ' (space)")
def rmse(file_y, file_y_hat, output, delim_y):

    sum_squared_error = 0
    n = 0

    for line_y, line_y_hat in itertools.zip_longest(file_y, file_y_hat):

        if line_y is None:
            msg = 'FILE_Y "{}" is shorter than FILE_Y_HAT "{}"'.format(
                file_y.name, file_y_hat.name)
            raise click.BadParameter(msg)

        if line_y_hat is None:
            msg = 'FILE_Y_HAT "{}" is shorter than FILE_Y "{}"'.format(
                file_y_hat.name, file_y.name)
            raise click.BadParameter(msg)

        y_hat = float(line_y_hat.rstrip())
        y = float(line_y.split(delim_y)[0])

        sum_squared_error += (y_hat - y) ** 2
        n += 1

    rmse_value = math.sqrt(sum_squared_error / n)

    output.write("{}\n".format(rmse_value))


@mpipe_cli.command(
    "metrics-reg",
    help="Compute regression metrics from predicted and observed values")
@click.argument(
    "file",
    type=click.File("r"),
    default="-")
@click.option(
    "--output", "-o", type=click.File("w"), default="-",
    help="output file")
@click.option(
    "--has-r2/--no-r2",
    default=False,
    help="compute R-squared also, requires two passes")
@click.option(
    "--delim", default=" ",
    help="FILE column delimiter, default: ' ' (space)")
def regression_metrics(file, output, has_r2, delim):
    r2, mean_y = None, None

    n = 0
    sum_y = 0

    if has_r2:
        if file.name == "-":
            msg = "Cannot compute R-squared from stdin, FILE must be a file"
            raise click.BadParameter(msg)

        for line in file:
            n += 1
            sum_y += float(line.split(delim)[1])
        mean_y = sum_y / n
        file.close()

    sum_abs_res = 0
    sum_squared_residuals = 0
    sum_squared_total = 0

    f = open(file.name) if has_r2 else file
    n = 0
    for line in f:

        split_line = line.split(delim)
        y = float(split_line[1])
        y_hat = float(split_line[0])

        abs_error = abs(y_hat - y)
        sum_abs_res += abs_error
        sum_squared_residuals += abs_error ** 2
        if has_r2:
            sum_squared_total += (y - mean_y) ** 2

        n += 1

    mse = sum_squared_residuals / n
    mae = sum_abs_res / n

    header = ["MSE", "MAE"]
    metrics = [mse, mae]

    if has_r2:
        r2 = 1 - (sum_squared_residuals / sum_squared_total)
        header += ["R2"]
        metrics += [r2]

    output.write("{}\n{}\n".format(
        ",".join(header),
        ",".join(map(str, metrics))))

    f.close()


@mpipe_cli.command(
    "metrics-cls",
    help="Compute classification metrics from observed and predicted values")
@click.argument(
    "file",
    type=click.Path(file_okay=True, dir_okay=False),
    default="-")
@click.option(
    "--output", "-o", type=str,
    help="output file")
@click.option(
    "--descriptive/--no-descriptive", default=True,
    help="use to also output descriptive stats of observed values")
def classification_metrics(file, output, descriptive):
    # awk code taken from
    # https://highonscience.wordpress.com/2015/03/08/fast-and-lean-ad-hoc-binary-classifier-evaluation/
    cmd = ("""
    awk -v OFS=$',' -v decimals=4 '
    BEGIN { max=10^decimals; min=1 }
    {
      score_bin=int(max*$2)
      if ($1 > 0) {
        positives[score_bin]++
      } else {
        negatives[score_bin]++
      }
      if ($2 >= 0.5) {
        if ($1 > 0) { tp++ } else { fp++ }
      } else {
        if ($1 > 0) { fn++ } else { tn++ }
      }
    }
    END {
      n=tp+fp
      ntot=tp+fp+tn+fn
      pos=tp+fn
      neg=tn+fp
	  if (pos > 0) {
         recall=tp/pos
      } else {
         recall="NaN"
      }
	  if (ntot > 0) {
         reach=n/ntot
      } else {
         reach="NaN"
      }
	  if (n > 0) {
         precision=tp/n
      } else {
         precision="NaN"
      }
      if (ntot > 0) {
         accuracy=(tp+tn)/ntot
      } else {
         accuracy="NaN"
      }
	  if ((2*tp+fp+fn) > 0) {
         f1score=2*tp/(2*tp+fp+fn)
      } else {
         f1score="NaN"
      }
	  if (reach > 0) {
         lift=recall/reach
      } else {
         lift="NaN"
      }
      ctp_prev=positives[max]
      cfp_prev=negatives[max]
      for (i = max-1; i >= min; i--) {
        ctp=ctp_prev+positives[i]
        cfp=cfp_prev+negatives[i]
        auc+=ctp*(cfp-cfp_prev)
        ctp_prev=ctp
        cfp_prev=cfp
      }
      auc_div=(ctp*cfp)
      if (auc_div > 0) {
         auroc=auc/auc_div
      } else {
         auroc="NaN"
      }
	  if (ntot > 0) {
         y_mean=pos/ntot
      } else {
         y_mean="NaN"
      }
    """ + (
       """
       print "n","n_pos","n_neg","y_mean","accuracy","precision","recall","f-measure","lift","auc"
       print ntot,pos,neg,y_mean,accuracy,precision,recall,f1score,lift,auroc
       """
       if descriptive else
       """
       print "accuracy","precision","recall","f-measure","lift","auc"
       print accuracy,precision,recall,f1score,lift,auroc
       """
    ) + "}' " + file + (" > {}".format(output) if output else ""))
	
    subprocess.call(cmd, shell=True, executable="/bin/bash")


@mpipe_cli.command(
    "describe-binary",
    help="compute simple descriptive statistics for a binary variable")
@click.argument(
    "file",
    type=click.Path(file_okay=True, dir_okay=False),
    default="-")
@click.option(
    "--output", "-o", type=str,
    help="output file")
def binary_variable_stats(file, output):
    file = "" if file == "-" else file

    cmd = """
    awk -v OFS=$',' -v decimals=4 '
    BEGIN { max=10^decimals; min=1 }
    {
      if ($1 > 0) { positives++ } else { negatives++ }
    }
    END {
      n=positives+negatives
      print "n","n_positive","n_negative","mean"
      print n,positives,negatives,positives/n
    }' """ + file + (" > {}".format(output) if output else "")

    subprocess.call(cmd, shell=True, executable="/bin/bash")


@mpipe_cli.command(
    "extract-column",
    help="Extract column from a csv file by column index of name")
@click.argument("column")
@click.argument("file", default="-")
@click.option(
    "--output", "-o", default="-",
    help="output file")
@click.option(
    "--delim", default="\t", callback=args_handlers.handle_column_delimiter,
    help="column delimiter, default: (tab)")
@click.option(
    "--header/--no-header", default=True,
    help="use if FILE contains header")
def extract_column(
        column,
        file,
        output,
        delim,
        header):
    file = "" if file == "-" else file

    if header:
        cmd = (
            'awk -F "{}" '.format(delim) +
            """'NR==1 {
                for (i=1; i<=NF; i++) {
                    ix[$i] = i
                }
            }
            NR>1 {
                print $ix[colname]
            }'""") + " colname={} {}".format(column, file)
    else:
        delim_arg = "-d {} ".format(delim) if delim != "\t" else ""
        cmd = "cut {}-f {} {}".format(delim_arg, column, file)

    if output and output != "-":
        cmd += " > {}".format(output)

    subprocess.call(cmd, shell=True, executable="/bin/bash")
