import sys
import subprocess

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Error: slicing parameters file not defined.")
        sys.exit(1)

    # train and predict
    cmd_main = " ".join([
        "python3",
        "main.py",
        sys.argv[1]
    ])
    subprocess.run(cmd_main, shell=True)
