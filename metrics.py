import sys
import os
import subprocess
import json


def mk_cmd_collect_metrics_output(slicing_name, country_id,
                                  L1=None, L2=None,
                                  learning_rate=None):
    cmd = " ".join([
        "ls",
        os.path.join(
            "data",
            "modelling",
            "metrics",
            slicing_name,
            "country_{}".format(country_id),
            "*",
            "metrics__day_*__l1_{}_l2_{}_lrate_{}.txt".format(
                L1 if L1 else "",
                L2 if L2 else "",
                learning_rate if learning_rate else "")),
        "| sort |",
        "mungy catcsv >",
        os.path.join(
            "data",
            "modelling",
            "metrics",
            slicing_name,
            "country_{}".format(country_id),
            "metrics__no_idx__all_l1_{}_l2_{}_lrate_{}.txt".format(
                L1 if L1 else "",
                L2 if L2 else "",
                learning_rate if learning_rate else ""))
    ])
    return cmd


# initialise parameters
slicing_params_file = sys.argv[1]

with open(slicing_params_file) as f:
    slicing_params = json.load(f)
country_id = slicing_params["country_id"]
n_training_periods = slicing_params["n_training_periods"]
training_period_type = slicing_params["training_period_type"]
n_testing_periods = slicing_params["n_testing_periods"]
testing_period_type = slicing_params["testing_period_type"]
n_window_periods = slicing_params["n_window_periods"]
window_period_type = slicing_params["window_period_type"]
min_testing_datasets = slicing_params["min_testing_datasets"]

with open("data/model_parameters/L1s.json") as f:
    L1s = json.load(f)
with open("data/model_parameters/L2s.json") as f:
    L2s = json.load(f)
with open("data/model_parameters/learning_rates.json") as f:
    learning_rates = json.load(f)

slicing_name = "train_{}{}_test_{}{}_by_{}{}".format(n_training_periods,
                                                     training_period_type[0],
                                                     min_testing_datasets,
                                                     testing_period_type[0],
                                                     n_window_periods,
                                                     window_period_type[0])

# collect metrics for later analysis
for L1 in L1s:
    for L2 in L2s:
        for learning_rate in learning_rates:
            cmd_collect_metrics = mk_cmd_collect_metrics_output(slicing_name,
                                                                country_id,
                                                                L1, L2,
                                                                learning_rate)
            subprocess.run(cmd_collect_metrics,
                           shell=True,
                           executable="/bin/bash")
