import collections
import datetime
import os
import operator

import pandas as pd
import dateutil.relativedelta


DatetimeInterval = collections.namedtuple(
    "DatetimeInterval",
    ["start", "end"])

DatetimeIntervalWithPaths = collections.namedtuple(
    "DatetimeIntervalWithPaths",
    ["interval", "filepaths"])

TrainTestDatasets = collections.namedtuple(
    "TrainTestDatasets",
    ["train_dataset", "test_datasets"])


def make_datetime_filename_pairs(filenames, datetime_parser=None):
    """
    Construct iterable of datetime.datetime and filename pairs (tuples).

    Parameters
    ----------
    filenames : Iterator[str]
        Iterable of filenames each containing datetime string
        as part of itself.
    datetime_parser : (str) -> datetime.datetime
        OPTIONAL
        Parses filename and returns datetime.datetime object.
        This function should take a single argument filename.
        If not specified, "%Y-%m-%d_%H%M" format is parsed from the
        last 15 characters of filename while ignoring filename extension.

    Returns
    -------
    Iterator[(datetime.datetime, str)]
        Iterable of sorted datetime.datetime and filename pairs (tuples)
        to be used for creation of training and testing datasets.
    """

    if datetime_parser is None:
        datetime_parser = _parse_datetime
    datetime_filename_pairs = sorted(
        [(datetime_parser(f), f,) for f in filenames],
        key=operator.itemgetter(0))
    return datetime_filename_pairs


def make_training_testing_datasets(
        datetime_filename_pairs,
        n_training_periods,
        training_period_type,
        n_testing_periods=1,
        testing_period_type='day',
        n_window_periods=1,
        window_period_type=None,
        datetime_from=None,
        datetime_to=None,
        min_testing_datasets=None):
    """
    Construct filenames' groups of multiple training and testing datasets
    with optional time window.

    Parameters
    ----------
    datetime_filename_pairs : Iterator[(datetime.datetime, str)]
        Iterable of datetime.datetime and filename pairs (tuples)
        to be used for creation of datasets.
    n_training_periods : int
        Number of periods to use for one training dataset.
    training_period_type : str
        One of {"hour", "day", "week", "month"}
        Period type of training dataset.
    n_testing_periods : int
        Optional
        Number of periods to use for one testing dataset.
    testing_period_type: str
        Optional
        One of {"hour", "day", "week", "month"}, optional
        Period type of testing dataset. Defaults to "day".
        Cannot be of greater granularity than the type of training dataset.
    n_window_periods : int
        Optional, default: 1
        Number of periods to use for training dataset window.
    window_period_type : str
        Optional
        One of {"hour", "day", "week", "month"}.
        Period type of window for dataset.
        If not set, training dataset period type is used which results
        in non-overlapping training datasets.
    datetime_from : datetime.datetime
        Optional 
        FROM filter for datasets' construction.
    datetime_to : datetime.datetime
        Optional 
        TO filter for datasets' construction.
    min_testing_datasets : int
        Optional
        Minimum number of testing datasets for each training dataset.
        If not set, this constrain is ignored which might results in a
        training dataset with no testing datasets.
    Returns
    -------
    list[
        (((datetime.datetime, datetime.datetime), list[str]), 
         list[((datetime.datetime, datetime.datetime), list[str])])
        ]

        List of pairs of training dataset and its testings datasets.
        Both training and testing datasets are pairs of the datasets' 
        interval and its filenames. Datasets' interval is a pair of 
        datetime.datetime objects.
    """

    if datetime_from is None:
        datetime_from = min([d for d, _ in datetime_filename_pairs])

    if datetime_to is None:
        datetime_to = max([d for d, _ in datetime_filename_pairs])

    if datetime_from >= datetime_to:
        raise ValueError("datetime_from must be less than datetime_to")

    if window_period_type is None:
        window_period_type = training_period_type

    training_intervals = _mk_training_intervals(
        datetime_from,
        datetime_to,
        n_training_periods,
        training_period_type,
        n_window_periods,
        window_period_type)

    training_testing_interval_pairs = _add_testing_intervals(
        training_intervals,
        n_testing_periods,
        testing_period_type,
        datetime_to,
        min_testing_datasets)

    training_testing_interval_pairs_with_paths = [
        (
            (
                (training_i_start, training_i_end,),
                _filter_filenames_by_daterange(
                    datetime_filename_pairs,
                    training_i_start,
                    training_i_end,
                    is_to_inclusive=False),
            ),
            [
                (
                    (testing_i_start, testing_i_end,),
                    _filter_filenames_by_daterange(
                        datetime_filename_pairs,
                        testing_i_start,
                        testing_i_end,
                        is_to_inclusive=False),
                )
                for testing_i_start, testing_i_end in testing_intervals
            ]
        )
        for (training_i_start, training_i_end), testing_intervals
        in training_testing_interval_pairs
    ]

    return training_testing_interval_pairs_with_paths


def name_training_testing_datasets(training_testing_interval_pairs_with_paths):
    """
    Convert the training_testing_interval_pairs_with_paths into named tuples
    
    Parameters
    ----------
    training_testing_interval_pairs_with_paths : 

    Returns
    -------
    list[TrainTestDatasets(
        train_dataset=DatetimeIntervalWithPaths(
            interval=DatetimeInterval(
                    start=datetime.datetime,
                    end=datetime.datetime),
            filepaths=list[str]),
        test_datasets=[
            DatetimeIntervalWithPaths(
                interval=DatetimeInterval(
                    start=datetime.datetime,
                    end=datetime.datetime),
                filepaths=list[str])
        ])]

    """

    return [
        TrainTestDatasets(
            train_dataset=DatetimeIntervalWithPaths(
                interval=DatetimeInterval(*train_interval),
                filepaths=train_fnames),
            test_datasets=[
                DatetimeIntervalWithPaths(
                    interval=DatetimeInterval(*test_interval),
                    filepaths=test_fnames)
                for test_interval, test_fnames in test_intervals
            ])
        for ((train_interval, train_fnames), test_intervals)
        in training_testing_interval_pairs_with_paths
    ]


def _mk_training_intervals(
        datetime_from,
        datetime_to,
        n_training_periods,
        training_period_type,
        n_window_periods=1,
        window_period_type='day'):
    pd_freq_type = _PeriodMap.translate_to_pd_period(training_period_type)

    period_start = _mk_starting_period_datetime(
        datetime_from, pd_freq_type)
    period_end = (
        period_start +
        _mk_period_timedelta(n_training_periods, training_period_type))
    training_window_delta = _mk_period_timedelta(
        n_window_periods, window_period_type)

    training_intervals = []
    next_start = period_start
    next_end = period_end - datetime.timedelta(seconds=1)
    while next_end <= datetime_to:
        training_intervals.append((next_start, next_end,))
        next_start += training_window_delta
        next_end += training_window_delta

    return training_intervals


def _add_testing_intervals(
        training_intervals,
        n_testing_periods,
        testing_period_type,
        datetime_to,
        min_testing_datasets):

    testing_pd_freq = "{}{}".format(
        n_testing_periods,
        _PeriodMap.translate_to_pd_period(testing_period_type))

    training_testing_interval_pairs = [
        (
            (tr_i_start, tr_i_end,),
            _mk_testing_intervals(
                tr_i_end + datetime.timedelta(seconds=1),
                datetime_to, testing_pd_freq),
        )
        for tr_i_start, tr_i_end in training_intervals
    ]

    if min_testing_datasets is not None:
        training_testing_interval_pairs = [
            i for i in training_testing_interval_pairs
            if len(i[1]) >= min_testing_datasets]

    return training_testing_interval_pairs


def _mk_starting_period_datetime(datetime_from, period_type):
    is_normalized = period_type != 'hour'
    return pd.date_range(
        start=datetime_from, normalize=is_normalized, periods=1,
        freq=period_type)[0].to_pydatetime()


def _mk_period_timedelta(n_periods, period_type):
    if period_type == 'month':
        return dateutil.relativedelta.relativedelta(months=n_periods)
    elif period_type == 'week':
        return dateutil.relativedelta.relativedelta(weeks=n_periods)
    elif period_type == 'day':
        return dateutil.relativedelta.relativedelta(days=n_periods)
    elif period_type == 'hour':
        return dateutil.relativedelta.relativedelta(hours=n_periods)


def _mk_testing_intervals(datetime_from, datetime_to, freq):
    testing_date_range = [
        dt_offset.to_pydatetime() for dt_offset in
        pd.date_range(start=datetime_from, end=datetime_to, freq=freq)]
    return [
        (s, e,) for s, e
        in zip(
            testing_date_range[:-1],
            [dt - datetime.timedelta(seconds=1)
             for dt in testing_date_range[1:]])]


def _filter_filenames_by_daterange(
        datetime_filename_pairs,
        datetime_from,
        datetime_to,
        is_from_inclusive=True,
        is_to_inclusive=False):

    def filter_from(dt):
        if is_from_inclusive:
            return datetime_from <= dt
        else:
            return datetime_from < dt

    def filter_to(dt):
        if is_to_inclusive:
            return dt <= datetime_to 
        else:
            return dt < datetime_to 

    return [
        fn for dt, fn in datetime_filename_pairs
        if filter_from(dt) and filter_to(dt)]


def _parse_datetime(filename):
    datetime_part = os.path.splitext(filename)[0][-15:]
    return datetime.datetime.strptime(datetime_part, "%Y-%m-%d_%H%M")


class _PeriodMap(object):
    period_types = ['hour', 'day', 'week', 'month']
    pd_freq_types = ['H', 'D', 'W-MON', 'MS']
    msg_type_template = "{} should be one of: {}. Given: {}"

    def __init__(self):
        pass

    @classmethod
    def translate_to_pd_period(cls, period_type):
        cls.validate_training_period_type(period_type)
        idx = cls.period_types.index(period_type)
        return cls.pd_freq_types[idx]

    @classmethod
    def get_valid_testing_period_types(cls, training_period_type):
        cls.validate_training_period_type(training_period_type)
        idx = cls.period_types.index(training_period_type) + 1
        return cls.period_types[:idx]

    @classmethod
    def validate_training_period_type(cls, training_period_type):
        if training_period_type not in cls.period_types:
            raise ValueError(cls.msg_type_template.format(
                "training_period_type",
                cls.period_types,
                training_period_type))

    @classmethod
    def validate_testing_period_type(
            cls,
            testing_period_type,
            training_period_type):
        valid_testing_period_types = cls.get_valid_testing_period_types(
            training_period_type)
        if testing_period_type not in valid_testing_period_types:
            raise ValueError(cls.msg_type_template.format(
                "testing_period_type with training_period_type '{}'".format(
                    training_period_type),
                valid_testing_period_types,
                testing_period_type))

