import collections
import csv
import datetime
import glob
import os
import random

import itertools


def split_2(
        file, output_1, output_2, probability,
        seed=None, skip_header=True, copy_header=False):
    """
    Split input file into two output files by given probability
    
    Parameters
    ----------
    file : io.TextIOWrapper
        input file
    output_1 : io.TextIOWrapper
        output file #1
    output_2 : io.TextIOWrapper
        output file #2
    probability : float
        probability to put line in output file #1
    seed : int
        pseudo-random number generator seed
    skip_header : bool
        skip first line if True
    copy_header : bool
        copy first line to output files if True

    Returns
    -------
    None

    """

    if seed:
        random.seed(seed)

    if copy_header:
        header = file.readline()
        output_1.write(header)
        output_2.write(header)
    elif skip_header:
        file.readline()

    for line in file:
        if random.random() < probability:
            output_1.write(line)
        else:
            output_2.write(line)


def split_by_probability(
        file,
        probabilities,
        filenames=None,
        dir_output=None,
        seed=None,
        skip_header=True,
        copy_header=True):
    """

    Parameters
    ----------
    file : io.TextIOWrapper
        input file
    probabilities : list[float]
        comma separated probabilities for each output file,
        last one is adjusted to make sum of probabilities equal to 1
    filenames : Optional[list[str]]
        optional comma separated filenames for each probability,
        must satisfy len(filenames) == len(probabilities)
    dir_output : Optional[str]
        optional output directory
    seed : int
        pseudo-random number generator seed
    skip_header : bool
        skip first line if True
    copy_header : bool
        copy first line to output files if True

    Returns
    -------
    None

    """

    if sum(probabilities[:-1]) > 1:
        msg = "sum of probabilities exceeded 1 even if ignoring the last one"
        raise ValueError(msg)
    probabilities[-1] = 1 - sum(probabilities[:-1])

    if filenames:
        if not len(filenames) == len(probabilities):
            msg = "filenames and probabilities have differing lengths: {} != {}".format(
                len(filenames), len(probabilities))
            raise ValueError(msg)
    else:
        counter = collections.Counter(probabilities)

        suffixes = itertools.chain.from_iterable(
            ["{}".format(p)] if n == 1 else ["{}_{}".format(p, i) for i in range(1, n+1)]
            for p, n in counter.items())
        fname_base, ext = os.path.splitext(os.path.basename(file.name))

        filenames = [
            "{}_{}{}".format(fname_base, s, ext)
            for s in suffixes]

    if dir_output:
        os.makedirs(dir_output, exist_ok=True)
        filenames = [os.path.join(dir_output, fname) for fname in filenames]

    cprobs = [0.0]
    for p in probabilities[:-1]:
        cprobs.append(p + cprobs[-1])

    prob_file_pairs = [
        (prop, open(fname, "w"))
        for prop, fname in zip(cprobs, filenames)]

    if seed:
        random.seed(seed)

    if copy_header:
        header = file.readline()
        for _, f in prob_file_pairs:
            f.write(header)
    elif skip_header:
        file.readline()

    for line in file:
        f = _choose_output_file(prob_file_pairs, random.random())
        f.write(line)

    for _, f in prob_file_pairs:
        f.close()


def _choose_output_file(prob_fname_pairs, prob):
    if len(prob_fname_pairs) == 1:
        return prob_fname_pairs[0][1]

    i = len(prob_fname_pairs) - 1
    while prob < prob_fname_pairs[i][0]:
        i -= 1

    return prob_fname_pairs[i][1]


def sample(file, output, probability, seed=None, has_header=False):
    """
    Sample file by given probability
    
    Parameters
    ----------
    file : io.TextIOWrapper
        input file
    output : io.TextIOWrapper
        output file
    probability : float
        probability to put line in sample
    seed : int
        pseudo-random number generator seed
    has_header : bool
        ignore first line if False

    Returns
    -------
    None

    """

    if seed:
        random.seed(seed)

    if has_header:
        header = file.readline()
        output.write(header)

    for line in file:
        if random.random() < probability:
            output.write(line)


def split_by_country(
        file, output_dir, prob_pos=None, prob_neg=None,
        label_column="impression_was_clicked", delim="\t"):
    """
    Split file by user country into separate files    
    
    Parameters
    ----------
    file : io.TextIOWrapper
        input file
    output_dir : str 
        output directory path
    prob_pos : float
        sampling probability of positive response
    prob_neg
        sampling probability of negative response
    label_column : str
        label column name for optional positive and/or negative sampling
    delim : str
        column delimiter

    Returns
    -------
    None

    """
    country_files_map = {}
    country_writers_map = {}

    reader = csv.DictReader(file, delimiter=delim)

    for line in reader:
        is_chosen = False
        country_id = line["user_country_id"]

        if prob_pos and line[label_column] == '1':
            if random.random() < prob_pos:
                is_chosen = True
        elif prob_neg and line[label_column] == '0':
            if random.random() < prob_neg:
                is_chosen = True
        else:
            is_chosen = True

        if is_chosen:
            if country_id not in country_writers_map:
                country_filename = os.path.join(
                    output_dir,
                    "user_country_{}.csv".format(country_id))

                country_files_map[country_id] = open(
                    country_filename, "w", newline="")

                country_writers_map[country_id] = csv.DictWriter(
                    country_files_map[country_id],
                    reader.fieldnames,
                    delimiter="\t")

                country_writers_map[country_id].writeheader()

            country_writers_map[country_id].writerow(line)

    for country_csv_file in country_files_map.values():
        country_csv_file.close()


def split_by_hour(file, output_dir, output_fname_base=None):
    """
    Split file by hour into separate files
    
    Parameters
    ----------
    file : io.TextIOWrapper
        input file
    output_dir : str
        output directory
    output_fname_base : str
        Base part of output filenames, original filename is used by default 
        with date suffixes appended

    Returns
    -------
    None

    """
    output_paths = {}

    fname_base = (
        output_fname_base or
        os.path.splitext(os.path.basename(file.name))[0])

    header = file.readline()

    for line in file:
        log_time_part = mk_log_time_part(parse_log_time(line[:13]))
        if log_time_part not in output_paths:
            output_path = mk_out_path(
                output_dir,
                fname_base,
                log_time_part)
            output_paths[log_time_part] = output_path

            with open(output_path, "w", encoding="utf-8") as f_output:
                f_output.write(header)

        with open(output_paths[log_time_part], "a",
                  encoding="utf-8") as f_output:
            f_output.write(line)


def split_countries_by_hour(
        dir_by_country, output_dir, output_fname_base):
    """
    Split each country file in dir_by_country by hour into separate files
    
    Parameters
    ----------
    dir_by_country : str
        directory with files conforming user_country_*.csv pattern
    output_dir : str
        output directory
    output_fname_base
        Base part of output filenames (original filename is used by default 
        with date suffixes appended)

    Returns
    -------
    None

    """

    datafile_pattern = os.path.join(
        dir_by_country,
        "user_country_*.csv")
    datafile_paths = list(glob.glob(datafile_pattern))

    for path in datafile_paths:
        with open(path, "r") as user_country_file:
            split_by_hour(user_country_file, output_dir, output_fname_base)


def parse_log_time(log_time):
    """
    return datetime from log_time string
    
    Parameters
    ----------
    log_time : str
        datetime string in "%Y-%m-%d %H" format

    Returns
    -------
    datetime.datetime

    """
    return datetime.datetime.strptime(log_time, "%Y-%m-%d %H")


def mk_out_path(dir_output, fname_base, log_time_part):
    """
    Return output file path with given fname_base and log_time 
    
    Parameters
    ----------
    dir_output : str
        output directory
    fname_base : str
        Base part of output filename
    log_time_part : str
        log time part

    Returns
    -------
    str

    """
    return os.path.join(
        dir_output,
        "{}_{}.csv".format(fname_base, log_time_part))


def mk_log_time_part(log_time):
    """
    Return log_time file name part
    
    Parameters
    ----------
    log_time : datetime.datetime
        log time datetime object

    Returns
    -------
    str

    """
    return "{}-{:02d}-{:02d}_{:02d}00".format(
        log_time.year, log_time.month, log_time.day,
        log_time.hour)
