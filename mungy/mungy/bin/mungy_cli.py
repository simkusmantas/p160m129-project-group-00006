import click

from mungy import (
    merging,
    slicing,
    vw_format,
)
from mungy.bin import args_handlers, cli_types


@click.group()
@click.version_option()
def mungy_cli():
    pass


@mungy_cli.command(
    "csv2vw", help="Convert data from csv to Vowpal Wabbit format")
@click.argument(
    "file", type=click.File("r"), default="-")
@click.option(
    "--output", "-o", type=click.File("w"), default="-",
    help="output file")
@click.option(
    "--delim", default="\t", callback=args_handlers.handle_column_delimiter,
    help="column delimiter, default: (tab)")
@click.option(
    "--nested-val-delims", default=";,",
    help="nested value delimiters, default: ;, (semicolon, comma)")
@click.option(
    "--label",
    help=("label column name if FILE has header, "
          "zero based label column index otherwise"))
@click.option(
    "--binary-label/--no-binary-label", default=False,
    help="use if label is binary")
@click.option(
    "--header/--no-header", default=True,
    help="use if FILE contains header")
@click.option(
    "--ignored-columns",
    help="comma separated list of ignored columns "
         "(names if FILE contains header, indices otherwise)")
@click.option(
    "--categorical-columns",
    help="comma separated list of categorical columns "
         "(names if FILE contains header, indices otherwise)")
@click.option(
    "--all-categorical/--not-all-categorical",
    default=False,
    help="use if all feature columns are categorical")
def csv2vw(
        file, output, delim, nested_val_delims, label,
        binary_label, header, ignored_columns, categorical_columns,
        all_categorical):

    ignored_columns = (
        ignored_columns.split(",")
        if ignored_columns
        else [])

    categorical_columns = (
        categorical_columns.split(",")
        if categorical_columns
        else [])

    if header is True:
        vw_format.csv2vw_with_header(
            file, output, delim, nested_val_delims, label,
            binary_label, ignored_columns, categorical_columns,
            all_categorical)
    else:
        vw_format.csv2vw_no_header(
            file, output, delim, nested_val_delims,
            args_handlers.handle_no_header_label(label),
            binary_label,
            args_handlers.handle_column_indices(
                "--ignored-columns", ignored_columns),
            args_handlers.handle_column_indices(
                "--categorical-columns", categorical_columns),
            all_categorical)


@mungy_cli.command(
    "catcsv", help="Concatenate csv files containing the same columns")
@click.argument(
    "file",
    nargs=-1)
@click.option(
    "--output", "-o",
    default="-",
    help="output file")
def catcsv(file, output):
    merging.cat_csv(file, output)


@mungy_cli.command(
    "split",
    help="Split FILE into separate files by given probabilities")
@click.argument("file", type=click.File("r"), default="-")
@click.argument("probabilities", type=cli_types.ProbabilitiesParamType())
@click.option(
    "--filenames", type=cli_types.FilenamesParamType(),
    help="output filenames, must be a filename for each probability")
@click.option(
    "-o", "--dir_output",
    type=click.Path(file_okay=False, dir_okay=True),
    help="output directory")
@click.option(
    "--seed", "-s", type=int,
    help="pseudo-random number generator seed")
@click.option(
    "--skip-header/--no-skip-header", default=False,
    help="ignore first line")
@click.option(
    "--copy-header/--no-copy-header", default=False,
    help="copy first line to output files")
def split(
        file,
        probabilities,
        filenames,
        dir_output,
        seed,
        skip_header,
        copy_header):

    slicing.split_by_probability(
        file,
        probabilities,
        filenames,
        dir_output,
        seed,
        skip_header,
        copy_header)


@mungy_cli.command(
    "split-2",
    help="Split FILE into two files by given probability")
@click.argument("file", type=click.File("r"), default="-")
@click.argument("output_1", type=click.File("w"))
@click.argument("output_2", type=click.File("w"))
@click.option("--probability", type=cli_types.ProbabilityParamType())
@click.option(
    "--seed", "-s", type=int,
    help="pseudo-random number generator seed")
@click.option(
    "--skip-header/--no-skip-header", default=False,
    help="ignore first line")
@click.option(
    "--copy-header/--no-copy-header", default=False,
    help="copy first line to output files")
def split_2(
        file, output_1, output_2, probability, seed,
        skip_header, copy_header):
    if not probability:
        probability = 0.5
    slicing.split_2(
        file, output_1, output_2, probability, seed,
        skip_header, copy_header)


@mungy_cli.command(
    "sample",
    help="Sample FILE by given probability")
@click.argument("probability", type=cli_types.ProbabilityParamType())
@click.argument("file", type=click.File("r"), default="-")
@click.option(
    "--output", "-o", type=click.File("w"), default="-",
    help="output file")
@click.option(
    "--seed", "-s", type=int,
    help="pseudo-random number generator seed")
@click.option(
    "--has-header/--no-header", default=False,
    help="ignore first line")
def sample(file, output, probability, seed, has_header):
    slicing.sample(file, output, probability, seed, has_header)


@mungy_cli.command(
    "split-by-country",
    help="Split FILE by user country into separate files")
@click.argument("file", type=click.File("r"), default="-")
@click.argument("output_dir", type=click.Path(exists=True, file_okay=False))
@click.option(
    "--prob-pos", type=cli_types.ProbabilityParamType(),
    help="sampling probability of positive response")
@click.option(
    "--prob-neg", type=cli_types.ProbabilityParamType(),
    help="sampling probability of negative response")
@click.option(
    "--label-column", default="impression_was_clicked",
    help="label column name for optional positive and/or negative sampling")
@click.option(
    "--delim", default="\t",
    callback=args_handlers.handle_column_delimiter,
    help="column delimiter, default: \\t (tab)")
def split_by_country(
        file, output_dir, prob_pos, prob_neg, label_column, delim):
    slicing.split_by_country(
        file, output_dir, prob_pos, prob_neg, label_column, delim)


@mungy_cli.command(
    "split-by-hour",
    help="Split FILE by hour into separate files")
@click.argument("file", type=click.File("r"), default="-")
@click.argument("output_dir", type=click.Path(exists=True, file_okay=False))
@click.option(
    "--output-fname-base",
    help="Base part of output filenames "
         "(original filename is used by default with date suffixes appended)")
def split_by_hour(file, output_dir, output_fname_base):
    slicing.split_by_hour(file, output_dir, output_fname_base)


@mungy_cli.command(
    "split-countries-by-hour",
    help="Split files DIR_BY_COUNTRY by hour into separate files")
@click.argument("dir_by_country",
                type=click.Path(exists=True, file_okay=False))
@click.argument("output_dir", type=click.Path(exists=True, file_okay=False))
@click.option(
    "--output-fname-base",
    help="Base part of output filenames "
         "(original filename is used by default with date suffixes appended)")
def split_countries_by_hour(dir_by_country, output_dir, output_fname_base):
    slicing.split_countries_by_hour(
        dir_by_country, output_dir,
        output_fname_base)
